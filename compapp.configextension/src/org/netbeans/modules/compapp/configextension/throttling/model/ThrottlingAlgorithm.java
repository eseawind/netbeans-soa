package org.netbeans.modules.compapp.configextension.throttling.model;


/**
 * @author ads
 *
 */
public class ThrottlingAlgorithm {

    public enum Algorithm {
        DISABLED("disabled", null),                             // NOI18N
        LEAKY_BUCKET("leakyBucket", "leakRate"),                // NOI18N
        TOKEN_BUCKET("tokenBucket", "maximumConcurrencyLimit"); // NOI18N
        
        private Algorithm( String attrName , String valueName ){
            myAttrName = attrName;
            myValueName = valueName;
        }
        
        @Override
        public String toString(){
            return myAttrName;
        }
        
        public String getValueName(){
            return myValueName;
        }
        
        public static Algorithm forString(String algorithmName ){
            for( Algorithm algorithm : values() ){
                if ( algorithm.toString().equals( algorithmName)){
                    return algorithm;
                }
            }
            return null;
        }
        
        private final String myAttrName;
        private final String myValueName; 
    }
    
    public ThrottlingAlgorithm(Algorithm algorithm , int value ){
        myAlgorithm = algorithm;
        myValue = value;
    }
    
    public Algorithm getAlgorithm(){
        return myAlgorithm;
    }
    
    public int getValue(){
        return myValue;
    }
    
    private Algorithm myAlgorithm;
    private int myValue;
}
