package net.openesb.netbeans.module.server.support.standalone.util;

import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.customizer.CustomizerSupport;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.platform.Specification;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.openide.modules.InstalledFileLocator;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneProperties {

    private static final Logger LOGGER = Logger.getLogger(StandaloneProperties.class.getName());

    /**
     * Java platform property which is used as a java platform ID
     */
    public static final String PLAT_PROP_ANT_NAME = "platform.ant.name"; //NOI18N

    private static final String PROP_URL = InstanceProperties.URL_ATTR;
    public static final String PROPERTY_ROOT_DIR = "root-dir";      //NOI18N
    public static final String PROPERTY_INSTANCE_NAME = "instance";      //NOI18N
    public static final String PROPERTY_ADMIN_PORT = "admin-port";      //NOI18N
    private static final String PROP_JAVA_PLATFORM = "java_platform";   // NOI18N
    private static final String PROP_JAVA_OPTS     = "java_opts";       // NOI18N
    private static final String PROP_DISPLAY_NAME = InstanceProperties.DISPLAY_NAME_ATTR;
    public static final String PROPERTY_HOST_NAME = "hostname";      //NOI18N
    private static final String PROP_USERNAME = InstanceProperties.USERNAME_ATTR;
    private static final String PROP_PASSWORD = InstanceProperties.PASSWORD_ATTR;
    private static final String PROP_SOURCES       = "sources";         // NOI18N
    private static final String PROP_JAVADOCS      = "javadocs";        // NOI18N
    
    // default values
    public static final String DEF_VALUE_HOST_NAME = "localhost";
    public static final String DEF_VALUE_INSTANCE_NAME = "server";
    private static final String DEF_VALUE_USERNAME = "admin";
    private static final String DEF_VALUE_PASSWORD = "admin";
    public static final int DEF_VALUE_ADMIN_PORT = 8699;
    public static final String DEF_VALUE_DISPLAY_NAME
	    = NbBundle.getMessage(StandaloneProperties.class, "LBL_DefaultDisplayName");
    private static final String  DEF_VALUE_JAVA_OPTS     = ""; // NOI18N
    
    private final InstanceProperties ip;
    private final StandaloneDeploymentManager dm;
    private File homeDir;

    private static final FilenameFilter CP_FILENAME_FILTER = new FilenameFilter() {

	@Override
	public boolean accept(File dir, String name) {
	    return name.endsWith(".jar") || new File(dir, name).isDirectory(); // NOI18N
	}
    };

    /**
     * Creates a new instance of StandaloneProperties
     */
    public StandaloneProperties(StandaloneDeploymentManager dm) {
	this.dm = dm;
	this.ip = dm.getInstanceProperties();

	String uri = ip.getProperty(PROP_URL); // NOI18N

	final String home = "home=";    // NOI18N
        
	int homeOffset = uri.indexOf(home) + home.length();
        String instanceHome=null;
        if (uri.indexOf(home)>0){
            instanceHome = uri.substring(homeOffset, uri.length());
        }

        final String hostname = "hostname=";    // NOI18N
        String instanceHostName=null;
	int hostnameOffset = uri.indexOf(hostname) + hostname.length();
        if (uri.indexOf(hostname)>0){
            instanceHostName = uri.substring(hostnameOffset, uri.length());
        }
        
	if (hostname== null && instanceHome == null) {
	    throw new IllegalArgumentException("OpenESB Standalone Home or Hostname must not be null."); // NOI18N
	}
        
        if (instanceHome!= null && instanceHome.length()>0){
            homeDir = new File(instanceHome);

            if (!homeDir.isAbsolute()) {
                InstalledFileLocator ifl = InstalledFileLocator.getDefault();
                homeDir = ifl.locate(instanceHome, null, false);
            }
            if (homeDir == null || !homeDir.exists()) {
                throw new IllegalArgumentException("OpenESB Standalone directory does not exist."); // NOI18N
            }
           }
    }

    public String getHostName(){
        String val = ip.getProperty(PROPERTY_HOST_NAME);
	return val != null && val.length() > 0 ? val
		: DEF_VALUE_HOST_NAME;
    }
    
        public void setHostName(String serverName) {
        ip.setProperty(PROPERTY_HOST_NAME, serverName);
    }
    
    public String getInstanceName() {
	String val = ip.getProperty(PROPERTY_INSTANCE_NAME);
	return val != null && val.length() > 0 ? val
		: DEF_VALUE_INSTANCE_NAME;
    }

    public String getDisplayName() {
	String val = ip.getProperty(PROP_DISPLAY_NAME);
	return val != null && val.length() > 0 ? val
		: DEF_VALUE_DISPLAY_NAME;
    }

    public String getUsername() {
	String val = ip.getProperty(PROP_USERNAME);
	return val != null && val.length() > 0 ? val
		: DEF_VALUE_USERNAME;
    }
    
    public void setUsername(String value) {
        ip.setProperty(PROP_USERNAME, value);
    }

    public String getPassword() {
	String val = ip.getProperty(PROP_PASSWORD);
	return val != null && val.length() > 0 ? val
		: DEF_VALUE_PASSWORD;
    }
    
    public void setPassword(String value) {
        ip.setProperty(PROP_PASSWORD, value);
    }

    public int getAdminPort() {
	String val = ip.getProperty(PROPERTY_ADMIN_PORT);

	if (val != null) {
	    try {
		return Integer.parseInt(val);
	    } catch (NumberFormatException nfe) {
		Logger.getLogger(StandaloneProperties.class.getName()).log(Level.INFO, null, nfe);
	    }
	}

	return DEF_VALUE_ADMIN_PORT;
    }

    public File getRootDir() {
	return homeDir;
    }
    
    public JavaPlatform getJavaPlatform() {
        String currentJvm = ip.getProperty(PROP_JAVA_PLATFORM);
        JavaPlatformManager jpm = JavaPlatformManager.getDefault();
        JavaPlatform[] installedPlatforms = jpm.getPlatforms(null, new Specification("J2SE", null)); // NOI18N
        for (int i = 0; i < installedPlatforms.length; i++) {
            String platformName = (String)installedPlatforms[i].getProperties().get(PLAT_PROP_ANT_NAME);
            if (platformName != null && platformName.equals(currentJvm)) {
                return installedPlatforms[i];
            }
        }
        // return default platform if none was set
        return jpm.getDefaultPlatform();
    }
    
    public void setJavaPlatform(JavaPlatform javaPlatform) {
        ip.setProperty(PROP_JAVA_PLATFORM, (String)javaPlatform.getProperties().get(PLAT_PROP_ANT_NAME));
    }
    
    public String getJavaOpts() {
        String val = ip.getProperty(PROP_JAVA_OPTS);
        return val != null ? val 
                           : DEF_VALUE_JAVA_OPTS;
    }
    
    public void setJavaOpts(String javaOpts) {
        ip.setProperty(PROP_JAVA_OPTS, javaOpts);
    }

    public List<URL> getClasses() {
	List<URL> list = new ArrayList<URL>();

	File rootDir = getRootDir();
   // 	File extLibDir = new File(rootDir, "lib" + File.separator + "ext");

	addFiles(new File(rootDir, "lib"), list); // NOI18N
   // 	addFiles(extLibDir, list); // NOI18N

	return list;
    }

    private void addFiles(File folder, List l) {
	File[] files = folder.listFiles(CP_FILENAME_FILTER);
	if (files == null) {
	    return;
	}
	Arrays.sort(files);

	// directories first
	List<File> realFiles = new ArrayList<File>(files.length);
	for (int i = 0; i < files.length; i++) {
	    if (files[i].isDirectory()) {
		addFiles(files[i], l);
	    } else {
		realFiles.add(files[i]);
	    }
	}
	for (File file : realFiles) {
	    try {
		l.add(Utils.fileToUrl(file));
	    } catch (MalformedURLException e) {
		LOGGER.log(Level.INFO, null, e);
	    }
	}
    }

    public List<URL> getJavadocs() {
	// OpenESB libs
	List<URL> retValue = new ArrayList<URL>();
	try {
	    retValue.add(new File(getRootDir(), "javaee.jar").toURI().toURL());
	} catch (MalformedURLException ex) {
	    Exceptions.printStackTrace(ex);
	}

	return retValue;
    }
    
    public void setJavadocs(List<URL> path) {
        ip.setProperty(PROP_JAVADOCS, CustomizerSupport.buildPath(path));
	// TODO: Fix this
        // dm.getstandalonePlatform().notifyLibrariesChanged();
    }
    
    public void setSources(List<URL> path) {
        ip.setProperty(PROP_SOURCES, CustomizerSupport.buildPath(path));
        // TODO: Fix this
        // dm.getstandalonePlatform().notifyLibrariesChanged();
    }

    public List<URL> getSources() {
	// OpenESB libs
	List<URL> retValue = new ArrayList<URL>();
	try {
	    retValue.add(new File(getRootDir(), "javaee.jar").toURI().toURL());
	} catch (MalformedURLException ex) {
	    Exceptions.printStackTrace(ex);
	}

	return retValue;
    }
}
