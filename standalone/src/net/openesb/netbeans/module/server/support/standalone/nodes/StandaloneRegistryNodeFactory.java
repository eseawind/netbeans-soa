package net.openesb.netbeans.module.server.support.standalone.nodes;

import java.util.logging.Logger;
import javax.enterprise.deploy.spi.Target;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import org.netbeans.modules.j2ee.deployment.plugins.spi.RegistryNodeFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneRegistryNodeFactory implements RegistryNodeFactory {

    private static final Logger LOGGER = Logger.getLogger(StandaloneRegistryNodeFactory.class.getName());

    /**
     * Creates a new instance of StandaloneRegistryNodeFactory
     */
    public StandaloneRegistryNodeFactory() {
    }

    /**
     * Return node representing the admin server. Children of this node are
     * filtered.
     *
     * @param lookup will contain DeploymentFactory, DeploymentManager,
     * Management objects.
     * @return admin server node.
     */
    @Override
    public Node getManagerNode(Lookup lookup) {
	StandaloneInstanceNode tn = new StandaloneInstanceNode(new Children.Map(), lookup);
	return tn;
    }

    /**
     * Provide node representing JSR88 Target object.
     *
     * @param lookup will contain DeploymentFactory, DeploymentManager, Target,
     * Management objects.
     * @return target server node
     */
    @Override
    public Node getTargetNode(Lookup lookup) {
	Target target = (Target) lookup.lookup(Target.class);
        StandaloneDeploymentManager depManager = (StandaloneDeploymentManager) lookup.lookup(StandaloneDeploymentManager.class);
                        
        if (depManager == null ) {
            System.out.println("WARNING: getManagerNode lookup returned "+depManager);//NOI18N
        }
        if (target == null ) {
            System.out.println("WARNING: getTargetNode lookup returned "+target);//NOI18N
            return null;
        }
        
        try{
            return new StandaloneContainerNode(depManager);
        } catch (Exception e){
            System.out.println("Cannot create the instance node in the " +
                    "factory " + e);//NOI18N
        }
        
        return null;//too bad
    }
}
