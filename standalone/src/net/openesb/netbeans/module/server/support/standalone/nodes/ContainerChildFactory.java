package net.openesb.netbeans.module.server.support.standalone.nodes;

import java.util.ArrayList;
import java.util.List;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import org.netbeans.modules.sun.manager.jbi.nodes.api.NodeExtensionProvider;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ContainerChildFactory {

    private final StandaloneDeploymentManager sdm;

    public ContainerChildFactory(StandaloneDeploymentManager sdm) {
	this.sdm = sdm;
    }

    public static final class WaitNode extends AbstractNode {

	public WaitNode() {
	    super(Children.LEAF);
	    setIconBase("org/netbeans/modules/java/navigation/resources/wait"); // NOI18N
	    setDisplayName(NbBundle.getMessage(ContainerChildFactory.class, "MSG_BadCredentials")); //NOI18N
	    setName(getClass().getName());
	}
    }

    public Object[] getChildrenObject() {
	if ((sdm == null) || (sdm.getMBeanServerConnection() == null)) {
	    return new Node[]{new WaitNode()};
	}

	return createRootChildren();
    }

    private Node[] createRootChildren() {
	List extNodes = getExtensionNodes();

	return (Node[]) extNodes.toArray(new Node[extNodes.size()]);
    }

    private List<Node> getExtensionNodes() {
	List<Node> nodes = new ArrayList<Node>();

	for (NodeExtensionProvider nep : Lookup.getDefault().lookupAll(NodeExtensionProvider.class)) {
	    if (nep != null) {
		Node node = nep.getExtensionNode(sdm.getMBeanServerConnection());
		if (node != null) {
		    nodes.add(node);
		}
	    }
	}

	return nodes;
    }
}
