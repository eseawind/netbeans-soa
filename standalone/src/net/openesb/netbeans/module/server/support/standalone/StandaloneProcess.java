package net.openesb.netbeans.module.server.support.standalone;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import net.openesb.netbeans.module.server.support.standalone.ide.StandaloneStartServer;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneProcess {
    private String standaloneHome;
    private Process process;

    public static final String TAG_EXEC_CMD = "openesb"; // NOI18N
    
    public void setStandaloneHome(String standaloneHome) {
        this.standaloneHome = standaloneHome;
    }

    public void start() throws ProcessCreationException, IOException {
        File workingDir = new File(standaloneHome);
        NbProcessDescriptor pd = createProcessDescriptor();

        if (pd == null) {
            throw new ProcessCreationException(null, "MSG_INVALID_JAVA", TAG_EXEC_CMD, "Process descriptor"); // NOI18N
        }

        System.out.println("Descriptor=" + pd);
        process = pd.exec(null, this.createEnvironment(), true, workingDir);
    }

    public OutputStream getOutputStream() {
        return process.getOutputStream();
    }

    public InputStream getInputStream() {
        return process.getInputStream();
    }

    public InputStream getErrorStream() {
        return process.getErrorStream();
    }

    public String[] createEnvironment() {
    //    ArrayList<String> envp = new ArrayList<String>();
    //    envp.add("OPENESB_HOME=" + jbossHome);
    //    return envp.toArray(new String[envp.size()]);
        return new String []{}; 
    }
    
    // private helper methods -------------------------------------------------    
    private static NbProcessDescriptor defaultExecDesc(String command, String argCommand) {
        return new NbProcessDescriptor(
                "{" + command + "}", // NOI18N
                "{" + argCommand + "}", // NOI18N
                NbBundle.getMessage(StandaloneStartServer.class, "MSG_StandaloneExecutionCommand"));
    }

    private NbProcessDescriptor createProcessDescriptor() throws ProcessCreationException {
        String startScript = standaloneHome + File.separator + "bin" + File.separator + TAG_EXEC_CMD;
        if (Utilities.isWindows()) {
            startScript += ".bat"; // NOI18N
        } else {
            startScript += ".sh";
        }

        File ss = new File(startScript);
        if (!ss.exists()) {
            throw new ProcessCreationException(null, "MSG_INVALID_JAVA", TAG_EXEC_CMD, startScript); // NOI18N
        }

        return new NbProcessDescriptor(startScript, "");
    }
}
