package net.openesb.netbeans.module.server.support.standalone.nodes;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneContainerNode extends AbstractNode implements Runnable, RefreshCookie {

    private static final RequestProcessor RP = new RequestProcessor("Refresher", 5);

    public static final class WaitNode extends AbstractNode {
        public WaitNode() {
            super(Children.LEAF);
            setIconBase("org/netbeans/modules/java/navigation/resources/wait"); // NOI18N
            setDisplayName(NbBundle.getMessage(StandaloneContainerNode.class,"WAITNODE")); //NOI18N
            setName(getClass().getName());
        }
    } // End of WaitNode class
    
    private final StandaloneDeploymentManager sdm;
    
    public StandaloneContainerNode(StandaloneDeploymentManager sdm) {
	super(getChildren(sdm));

	this.sdm = sdm;
	
	javax.swing.SwingUtilities.invokeLater(new Runnable() {
	    @Override
	    public void run() {
		refresh();
	    }
	});
    }

    @Override
    public void run() {
	javax.swing.SwingUtilities.invokeLater(new Runnable() {
	    @Override
	    public void run() {
		setChildren(new ContainerChildren(sdm));
		ContainerChildren ch = (ContainerChildren)getChildren();
		ch.updateKeys();
	    }
	});
    }

    @Override
    public Action[] getActions(boolean flag) {
        return new SystemAction[] {};
    }

    @Override
    public void refresh() {
	RP.post(this);
    }
    
    /**
     *
     */
    static Children getChildren(final StandaloneDeploymentManager controller){
        return new ContainerChildren(controller);
    }
    
    /**
     *
     *
     */
    public static class ContainerChildren extends Children.Keys {
        ContainerChildFactory cfactory;
        public ContainerChildren(StandaloneDeploymentManager controller) {
            if(controller == null) {
                Logger.getLogger(this.getClass().getName()).log(Level.FINE, "Controller for child factory " +
                    "is null");
            }
            this.cfactory = new ContainerChildFactory(controller);
        }
	
        protected void addNotify() {
            
            Node n[]= new Node[1];
            n[0]=new WaitNode();
            setKeys(n);
            RequestProcessor.getDefault().post(new Runnable() {
                public void run() {
                    try {
                        setKeys(cfactory.getChildrenObject());
                    } catch (RuntimeException e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.FINE, e.getMessage(), e);
                    }
                }
            });    
        }
	
        protected void removeNotify() {
            setKeys(java.util.Collections.EMPTY_SET);
        }
	
        public void updateKeys() {
            refresh();
        }
	
        protected org.openide.nodes.Node[] createNodes(Object obj) {
            try {
                return new Node[] { (Node)obj };
            } catch(RuntimeException rex) {
                return new Node[] {};
            } catch(Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.FINE, e.getMessage(), e);
                return new Node[] {};
            }
        }
    }
}
