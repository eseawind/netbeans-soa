package net.openesb.netbeans.module.server.support.standalone.nodes;

import org.openide.nodes.Node.Cookie;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface RefreshCookie extends Cookie {
    
    /** Refreshes the children of the node */
    public void refresh();
    
}
