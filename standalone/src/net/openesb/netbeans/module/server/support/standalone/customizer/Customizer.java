package net.openesb.netbeans.module.server.support.standalone.customizer;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import org.openide.util.NbBundle;

/**
 *
 * OpenESB Standalone instance customizer which is accessible from server manager.
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class Customizer extends JTabbedPane {

    private final StandaloneDeploymentManager manager;

    public Customizer(StandaloneDeploymentManager aManager) {
        manager = aManager;
        initComponents ();
    }
    
    private void initComponents() {
        getAccessibleContext().setAccessibleName (NbBundle.getMessage(Customizer.class,"ACS_Customizer")); // NOI18N
        getAccessibleContext().setAccessibleDescription (NbBundle.getMessage(Customizer.class,"ACS_Customizer")); // NOI18N
        CustomizerDataSupport custData = new CustomizerDataSupport(manager);
        // set help ID according to selected tab
        addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                String helpID = null;
                switch (getSelectedIndex()) {
                    case 0 : helpID = "tomcat_customizer_general";    // NOI18N
                             break;
                    case 1 : helpID = "tomcat_customizer_startup";    // NOI18N
                             break;
                    case 2 : helpID = "tomcat_customizer_platform";    // NOI18N
                             break;
                    case 3 : helpID = "tomcat_customizer_deployment";   // NOI18N
                             break;
                    case 4 : helpID = "tomcat_customizer_classes";    // NOI18N
                             break;
                    case 5 : helpID = "tomcat_customizer_sources";    // NOI18N
                             break;
                    case 6 : helpID = "tomcat_customizer_javadoc";    // NOI18N
                             break;
                }
                putClientProperty("HelpID", helpID); // NOI18N
            }
        });

        addTab(NbBundle.getMessage(Customizer.class,"TXT_General"),
                new CustomizerGeneral(custData));
        addTab(NbBundle.getMessage(Customizer.class,"TXT_Startup"),
                new CustomizerJVM(custData));
        addTab(NbBundle.getMessage(Customizer.class,"TXT_Tab_Classes"), 
               CustomizerSupport.createClassesCustomizer(custData.getClassModel()));
        addTab(NbBundle.getMessage(Customizer.class,"TXT_Tab_Sources"), 
                CustomizerSupport.createSourcesCustomizer(custData.getSourceModel(), null));
        addTab(NbBundle.getMessage(Customizer.class,"TXT_Tab_Javadoc"), 
                CustomizerSupport.createJavadocCustomizer(custData.getJavadocsModel(), null));
    }
}
