package net.openesb.netbeans.module.server.support.standalone.nodes.actions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.nodes.StandaloneInstanceNode;
import org.openide.awt.HtmlBrowser;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.NodeAction;

/**
 * Action which opens the OpenESB web console in a browser.
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ViewWebConsoleAction extends NodeAction {

    @Override
    protected void performAction (Node[] nodes) {
	for (Node node : nodes) {
	    final StandaloneInstanceNode cookie = (StandaloneInstanceNode) node.getCookie(StandaloneInstanceNode.class);
	    if (cookie != null) {
		RequestProcessor.getDefault().post(new Runnable() {
		    public void run() {
			StandaloneDeploymentManager sdm = cookie.getStandaloneDeploymentManager();
			String adminUrl = sdm.getServerUri() + "/webui/index.html"; // NOI18N
			try {
			    HtmlBrowser.URLDisplayer.getDefault().showURL(new URL(adminUrl));
			} catch (MalformedURLException e) {
			    Logger.getLogger(ViewWebConsoleAction.class.getName()).log(Level.INFO, null, e);
			}
		    }
		});
	    }
	}
    }

    @Override
    protected boolean enable (Node[] nodes) {
	for (Node node : nodes) {
	    StandaloneInstanceNode cookie = (StandaloneInstanceNode) node.getCookie(StandaloneInstanceNode.class);
	    if (cookie == null) {
		return false;
	    }
	}
        return true;
    }

    @Override
    public String getName () {
        return NbBundle.getMessage(ViewWebConsoleAction.class, "LBL_WebConsoleAction");
    }
    
    @Override
    protected boolean asynchronous() {
        return false;
    }

    @Override
    public HelpCtx getHelpCtx () {
        return HelpCtx.DEFAULT_HELP;
    }
}
