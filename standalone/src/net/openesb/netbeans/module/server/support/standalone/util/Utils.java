package net.openesb.netbeans.module.server.support.standalone.util;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import org.openide.filesystems.FileUtil;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public final class Utils {

    /**
     * Creates a new instance of Utils
     */
    private Utils() {
        super();
    }

    /**
     * Return true if the specified port is free, false otherwise.
     */
    public static boolean isPortFree(int port) {
        try {
            ServerSocket soc = new ServerSocket(port);
            try {
                soc.close();
            } finally {
                return true;
            }
        } catch (IOException ioe) {
            return false;
        }
    }

    /**
     * Return true if an OpenESB Standalone server is running on the specifed
     * port
     */
    public static boolean ping(String hostname,int port) {
        Socket socket = new Socket();
        try {
            try {
                socket.connect(new InetSocketAddress(hostname, port)); // NOI18N
                return true;
            } finally {
                socket.close();
            }
        } catch (IOException ioe) {
            return false;
        }
    }
    
    /**
     * Creates URL for the file to be used for classpath. This means for
     * jar and zip file method will return jar: URL for other files or
     * directories it will return file: URL.
     *
     * @param file the file to get URL for
     * @return the proper URL
     * @throws MalformedURLException thrown if URL could not be constructed
     * @since 1.70
     */
    public static URL fileToUrl(File file) throws MalformedURLException {
        URL url = file.toURI().toURL();
        if (!file.isDirectory()) {
            if (file.getName().endsWith(".zip") || file.getName().endsWith("jar")) {
            // isArchiveFile reads the bytes from file which is forbidden
            // to be done from UI - check fro extensions should be safe enough
            // see #207440
            //if (FileUtil.isArchiveFile(url)) {
                url = FileUtil.getArchiveRoot(url);
            }
        }
        return url;
    }
}
