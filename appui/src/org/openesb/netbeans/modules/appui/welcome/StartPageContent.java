package org.openesb.netbeans.modules.appui.welcome;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author David BRASSELY
 */
public class StartPageContent extends JPanel implements Constants {

    public StartPageContent() {
        super( new GridBagLayout() );
        
        setBackground( Utils.getColor( COLOR_SCREEN_BACKGROUND ) );
        setMinimumSize( new Dimension(START_PAGE_MIN_WIDTH,100) );
        
        add( new JLabel(), new GridBagConstraints(0,0,1,1,0.0,1.0,
                GridBagConstraints.CENTER,GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0) );
        add( createMainPanel(), new GridBagConstraints(0,1,1,1,0.0,0.0,
                GridBagConstraints.CENTER,GridBagConstraints.NONE, new Insets(10,10,10,10), 0,0) );
        add( new JLabel(), new GridBagConstraints(0,2,1,1,0.0,1.0,
                GridBagConstraints.CENTER,GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0) );
        add( new ShowNextTime(), new GridBagConstraints(0,3,1,1,0.0,0.0,
                GridBagConstraints.SOUTH,GridBagConstraints.NONE, new Insets(10,10,20,10), 0,0) );
    }
    
    private JComponent createMainPanel() {
        JPanel res = new JPanel();
        res.setOpaque(false);
        res.setLayout(new BorderLayout());
        
        res.add(new CaptionPanel(), BorderLayout.NORTH);
        res.add(new ContentsPanel(), BorderLayout.CENTER);
        res.add(new FooterPanel(), BorderLayout.SOUTH);
        
        return res;
    }
}
